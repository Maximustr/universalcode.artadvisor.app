package com.example.universalcodeartadvisorapp.Activitys;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.universalcodeartadvisorapp.Common.BitMap;
import com.example.universalcodeartadvisorapp.Common.CurrentUser;
import com.example.universalcodeartadvisorapp.Common.Image;
import com.example.universalcodeartadvisorapp.R;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.content.ContentValues.TAG;

public class UpdateProfile extends AppCompatActivity {

    private static FirebaseFirestore db = FirebaseFirestore.getInstance();
    private static int RESULT_LOAD_IMAGE = 1;
    private UpdateProfile self = this;
    private Bitmap avatarPhoto;
    private boolean isNewPhoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ImageView buttonLoadImage = findViewById(R.id.ProfileUpdatePhoto);
        Picasso.get().load(CurrentUser.user.getPhotoUrlToDownload()).into(buttonLoadImage);
        FillDropDown(findViewById(R.id.NationallityInput));

        TextInputEditText nameInput = findViewById(R.id.account_nameTxttBox);
        TextInputEditText lastNameInput = findViewById(R.id.account_LastNamelTxttBox);
        nameInput.setText(CurrentUser.user.getName());
        lastNameInput.setText(CurrentUser.user.getLastName());

        buttonLoadImage.setOnClickListener(arg0 -> {
            if (ActivityCompat.checkSelfPermission(UpdateProfile.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(UpdateProfile.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, RESULT_LOAD_IMAGE);
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
                ImageView imageView = findViewById(R.id.ProfileImageCreateAccount);
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.TRANSPARENT);
                gd.setCornerRadius(15f);
                gd.setStroke((int) 1f, Color.BLACK);
                imageView.setBackground(gd);
            } else {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = findViewById(R.id.ProfileUpdatePhoto);
            avatarPhoto = BitMap.getCircleBitmap1(BitmapFactory.decodeFile(picturePath));
            imageView.setImageBitmap(avatarPhoto);
            isNewPhoto = true;
        }
    }

    private boolean IsValidData() {
        AtomicBoolean isValidData = new AtomicBoolean(true);

        TextInputEditText nameInput = findViewById(R.id.account_nameTxttBox);
        TextInputEditText lastNameInput = findViewById(R.id.account_LastNamelTxttBox);

        ArrayList<EditText> Elements = new ArrayList<>();
        Elements.add(nameInput);
        Elements.add(lastNameInput);

        Elements.forEach(x -> {
            if ("".equals(x.getText().toString())) {
                String errorString = "Este campo es requerido";
                x.setError(errorString);
                isValidData.set(false);
            } else {
                x.setError(null);
            }
        });

        return isValidData.get();
    }


    public void FillDropDown(final Spinner sItems) {
        db.collection("keyVal")
                .whereEqualTo("Group", "CountriesCodes")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && !task.getResult().getDocuments().isEmpty()) {
                        ArrayList<String> listNationalities = new ArrayList<>();
                        task.getResult().getDocuments().forEach(x -> listNationalities.add((String) x.get("Value")));
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(self, android.R.layout.simple_spinner_item, listNationalities);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sItems.setAdapter(adapter);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        int spinnerPosition = adapter.getPosition(CurrentUser.user.getNationality());
                        sItems.setSelection(spinnerPosition);
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }

    public void UpdateAccount(View view) {
        if (IsValidData()) {
            TextInputEditText nameInput = findViewById(R.id.account_nameTxttBox);
            TextInputEditText lastNameInput = findViewById(R.id.account_LastNamelTxttBox);
            Spinner nationallity = findViewById(R.id.NationallityInput);
            ConstraintLayout spinner = findViewById(R.id.containerLoading);
            spinner.setVisibility(View.VISIBLE);
            db.collection("users").whereEqualTo("email", CurrentUser.user.getEmail()).get().addOnSuccessListener(x -> {
                DocumentSnapshot user = x.getDocuments().get(0);
                CurrentUser.user.setNationality(nationallity.getSelectedItem().toString());
                CurrentUser.user.setLastName(lastNameInput.getText().toString());
                CurrentUser.user.setName(nameInput.getText().toString());

                if (isNewPhoto) {
                    String UserPhoto = "Fotos/Avatars/" + UUID.randomUUID().toString();
                    CurrentUser.user.setPhotoUrl(UserPhoto);

                    Image.UploadPhoto(avatarPhoto, UserPhoto, db.collection("users").document(user.getId()).update("lastName", CurrentUser.user.getLastName(),
                            "name", CurrentUser.user.getName(),
                            "photoUrl", CurrentUser.user.getPhotoUrl(),
                            "nationality", CurrentUser.user.getNationality()).addOnSuccessListener(aVoid -> self.finish()
                    ));
                } else {
                    db.collection("users").document(user.getId()).update("lastName", CurrentUser.user.getLastName(),
                            "name", CurrentUser.user.getName(),
                            "nationality", CurrentUser.user.getNationality()).addOnSuccessListener(aVoid ->
                            self.finish()
                    );
                }
            });
        }
    }

    public void Cancel(View view) {
        this.finish();
    }
}
