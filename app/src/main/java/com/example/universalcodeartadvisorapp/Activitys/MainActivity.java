package com.example.universalcodeartadvisorapp.Activitys;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.example.universalcodeartadvisorapp.Fragments.MapFragment;
import com.example.universalcodeartadvisorapp.Fragments.Museum_fragment_list;
import com.example.universalcodeartadvisorapp.Fragments.Museum_fragment_profile;
import com.example.universalcodeartadvisorapp.Fragments.MyAccount;
import com.example.universalcodeartadvisorapp.R;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView mMainNav;
    private Toolbar toolbar;
    private FrameLayout mMainFrame;
    private Museum_fragment_list museumFragment;
    private Museum_fragment_profile museumProfile;
    private MyAccount myAccount;

    private MapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainFrame = findViewById(R.id.main_frame);
        mMainNav = findViewById(R.id.main_nav);
        toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        getSupportActionBar().hide();
        museumFragment = new Museum_fragment_list();
        mapFragment = new MapFragment();
        museumProfile = new Museum_fragment_profile();
        setFragment(mapFragment);

        mMainNav.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {

                case R.id.nav_map:
                    setFragment(mapFragment);
                    return true;

                case R.id.nav_museos:
                    getSupportActionBar().show();
                    setFragment(museumFragment);
                    return true;

                case R.id.nav_perfil:
                    getSupportActionBar().show();
                    myAccount = new MyAccount();
                    setFragment(myAccount);
                    return true;

                default:
                    return false;
            }
        });

        setFragment(mapFragment);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_search:
//                    item.setVisible(View.GONE);
                break;
//            case R.id.nav_back:
//                setFragment(museumFragment);
//                break;

        }

        return true;
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_items, menu);

        return true;
    }

    public void SignOut(View view) {
        myAccount.SignOut(view);
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    public void EditProfile(View view) {
        myAccount.EditProfile(view);
    }
}
