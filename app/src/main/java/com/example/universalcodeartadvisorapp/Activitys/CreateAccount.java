package com.example.universalcodeartadvisorapp.Activitys;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.universalcodeartadvisorapp.Common.BitMap;
import com.example.universalcodeartadvisorapp.Common.CurrentUser;
import com.example.universalcodeartadvisorapp.Common.Image;
import com.example.universalcodeartadvisorapp.Common.KeywordHelper;
import com.example.universalcodeartadvisorapp.Pojos.User;
import com.example.universalcodeartadvisorapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.content.ContentValues.TAG;

public class CreateAccount extends AppCompatActivity {

    private static FirebaseFirestore db = FirebaseFirestore.getInstance();
    private static int RESULT_LOAD_IMAGE = 1;
    private CreateAccount self = this;
    private Bitmap avatarPhoto;
    private FirebaseAuth mAuth;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        FillDropDown(findViewById(R.id.NationallityInput));

        ImageView buttonLoadImage = findViewById(R.id.ProfileImageCreateAccount);
        buttonLoadImage.setOnClickListener(arg0 -> {
            if (ActivityCompat.checkSelfPermission(CreateAccount.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(CreateAccount.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, RESULT_LOAD_IMAGE);
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
                ImageView imageView = findViewById(R.id.ProfileImageCreateAccount);
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.TRANSPARENT);
                gd.setCornerRadius(15f);
                gd.setStroke((int) 1f, Color.BLACK);
                imageView.setBackground(gd);
            } else {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        mAuth = FirebaseAuth.getInstance();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = findViewById(R.id.ProfileImageCreateAccount);
            avatarPhoto = BitMap.getCircleBitmap1(BitmapFactory.decodeFile(picturePath));
            imageView.setImageBitmap(avatarPhoto);
        }
    }

    public void CreateAccount(View view) {
        KeywordHelper.hideKeyboard(this);

        if (IsValidData()) {
            ConstraintLayout spinner = findViewById(R.id.containerLoading);
            spinner.setVisibility(View.VISIBLE);
            TextInputEditText nameInput = findViewById(R.id.account_nameTxttBox);
            TextInputEditText lastNameInput = findViewById(R.id.account_LastNamelTxttBox);
            TextInputEditText emailInput = findViewById(R.id.account_emailTxttBox);
            ShowHidePasswordEditText passswordInput = findViewById(R.id.accountPassWordTxttBox);
            Spinner nationallity = findViewById(R.id.NationallityInput);

            User newUser = new User();
            newUser.setEmail(emailInput.getText().toString());
            newUser.setLastName(lastNameInput.getText().toString());
            newUser.setName(nameInput.getText().toString());
            newUser.setPassword(passswordInput.getText().toString());
            newUser.setNationality(nationallity.getSelectedItem().toString());

            mAuth.createUserWithEmailAndPassword(newUser.getEmail(), newUser.getPassword())
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            String UserPhoto = "Fotos/Avatars/" + UUID.randomUUID().toString();
                            Image.UploadPhoto(avatarPhoto, UserPhoto);
                            FirebaseUser user = mAuth.getCurrentUser();
                            newUser.setIdUser(user.getUid());
                            newUser.setPhotoUrl(UserPhoto);
                            db.collection("users").add(newUser).addOnCompleteListener((x) -> CurrentUser.GetCurrentUser(self));
                        } else {
                            // If sign in fails, display a message to the user.
                            spinner.setVisibility(View.GONE);
                            if (task.getException() instanceof com.google.firebase.auth.FirebaseAuthUserCollisionException) {
                                Toast.makeText(this, "Este Usuario ya está registrado.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(this, "Algo pasó con su registro, por favor, intentelo luego.", Toast.LENGTH_SHORT).show();
                            }
                        }

                    });
        }
    }

    private boolean IsValidData() {
        AtomicBoolean isValidData = new AtomicBoolean(true);

        TextInputEditText nameInput = findViewById(R.id.account_nameTxttBox);
        TextInputEditText lastNameInput = findViewById(R.id.account_LastNamelTxttBox);
        TextInputEditText emailInput = findViewById(R.id.account_emailTxttBox);
        ShowHidePasswordEditText passswordInput = findViewById(R.id.accountPassWordTxttBox);
        ShowHidePasswordEditText confirmPassWordInput = findViewById(R.id.account_confirmPassWordTxttBox);

        ArrayList<EditText> Elements = new ArrayList<>();
        Elements.add(nameInput);
        Elements.add(lastNameInput);
        Elements.add(emailInput);
        Elements.add(passswordInput);
        Elements.add(confirmPassWordInput);

        Elements.forEach(x -> {
            if ("".equals(x.getText().toString())) {
                String errorString = "Este campo es requerido";
                x.setError(errorString);
                isValidData.set(false);
            } else {
                x.setError(null);
            }
        });

        if (!passswordInput.getText().toString().equals(confirmPassWordInput.getText().toString())) {
            confirmPassWordInput.setError("Las contraseñas no coinciden.");
            isValidData.set(false);
        }

        if (avatarPhoto == null) {
            isValidData.set(false);
            Toast.makeText(this, "Necesita registrar una foto de perfil.", Toast.LENGTH_SHORT).show();
        }

        return isValidData.get();
    }

    public void FillDropDown(final Spinner sItems) {
        db.collection("keyVal")
                .whereEqualTo("Group", "CountriesCodes")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && !task.getResult().getDocuments().isEmpty()) {
                        ArrayList<String> listNationalities = new ArrayList<>();
                        task.getResult().getDocuments().forEach(x -> listNationalities.add((String) x.get("Value")));
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(self, android.R.layout.simple_spinner_item, listNationalities);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sItems.setAdapter(adapter);
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }
}
