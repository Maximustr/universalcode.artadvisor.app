package com.example.universalcodeartadvisorapp.Activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.example.universalcodeartadvisorapp.Common.CurrentUser;
import com.example.universalcodeartadvisorapp.Common.KeywordHelper;
import com.example.universalcodeartadvisorapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

public class LogIn extends AppCompatActivity {

    final LogIn self = this;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        mAuth = FirebaseAuth.getInstance();
        CurrentUser.GetCurrentUser(self);
    }

    public void LogInClick(View view) {
        KeywordHelper.hideKeyboard(this);
        TextInputEditText emailInput = findViewById(R.id.userNameTextBox);
        ShowHidePasswordEditText passWordInput = findViewById(R.id.passwordTextBox);
        String email = emailInput.getText().toString();
        String password = passWordInput.getText().toString();

        if (email.equals("") || password.equals("")) {
            Toast.makeText(this, "Por favor llenar todos los campos.", Toast.LENGTH_SHORT).show();
        } else {
            LogInProcess(email, password);
        }
    }

    private void LogInProcess(String email, String password) {
        ConstraintLayout spinner = findViewById(R.id.containerLoading);
        spinner.setVisibility(View.VISIBLE);

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        CurrentUser.GetCurrentUser(self);
                    } else {
                        ConstraintLayout spinner1 = findViewById(R.id.containerLoading);
                        spinner1.setVisibility(View.GONE);
                        // If sign in fails, display a message to the user.
                        Toast.makeText(LogIn.this, "Usuario o clave de accesso incorrectos.", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public void ForgotPassword(View view) {

        KeywordHelper.hideKeyboard(this);
        TextInputEditText emailInput = findViewById(R.id.userNameTextBox);
        String email = emailInput.getText().toString();

        if (email.equals("")) {
            Toast.makeText(this, "Por favor llenar el campo de correo.", Toast.LENGTH_SHORT).show();
        } else {
            final ConstraintLayout spinner = findViewById(R.id.containerLoading);
            spinner.setVisibility(View.VISIBLE);
            mAuth.sendPasswordResetEmail(email)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Toast.makeText(LogIn.this, "Se ha enviado un correo para restaurar la contraseña.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LogIn.this, "Ha habido un problema enviando el correo.", Toast.LENGTH_SHORT).show();
                        }
                        spinner.setVisibility(View.GONE);
                    });
        }
    }

    public void CreateAccountClick(View view) {
        Intent intent = new Intent(this, CreateAccount.class);
        startActivity(intent);
    }
}
