package com.example.universalcodeartadvisorapp.Common;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.universalcodeartadvisorapp.Activitys.MainActivity;
import com.example.universalcodeartadvisorapp.Pojos.User;
import com.example.universalcodeartadvisorapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

import static android.content.ContentValues.TAG;

public class CurrentUser {

    static public User user;

    public static User GetCurrentUser(final AppCompatActivity self) {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final ConstraintLayout spinner = self.findViewById(R.id.containerLoading);

        spinner.setVisibility(View.VISIBLE);
        FirebaseUser fireBaseUser = mAuth.getCurrentUser();
        db.collection("users")
                .whereEqualTo("email", fireBaseUser != null ? fireBaseUser.getEmail() : "")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && !task.getResult().getDocuments().isEmpty()) {
                        Map<String, Object> a = task.getResult().getDocuments().get(0).getData();
                        user = new User(a);
                        spinner.setVisibility(View.GONE);

                        Intent intent = new Intent(self, MainActivity.class);
                        self.startActivity(intent);
                        self.finish();
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                        spinner.setVisibility(View.GONE);
                    }
                });
        return user;
    }
}

