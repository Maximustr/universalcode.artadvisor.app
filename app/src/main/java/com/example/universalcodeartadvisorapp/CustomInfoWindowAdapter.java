package com.example.universalcodeartadvisorapp;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter,View.OnClickListener {

    private final View mWindow;


    private Context context;
    private String url;
    private View.OnClickListener listener;

    private Uri filePaht;
    Marker marker;

    public CustomInfoWindowAdapter(Context context) {
        this.context = context;

        this.url = url;
        this.mWindow = LayoutInflater.from(this.context).inflate(R.layout.custom_address,null);
    }

    private void rendowWindowText(Marker marker, View view){
        String tittle = marker.getTitle();
        String p = url;
        TextView textViewTittle =  (TextView)view.findViewById(R.id.title);
        ImageView imageView = (ImageView)view.findViewById(R.id.idMuseum);
        String urls = marker.getSnippet();
        view.setOnClickListener(this);
        //Picasso.get().load(urls);
        //Float prueba = marker.getZIndex();

//        ProgressBar spinner = view.findViewById(R.id.progressBar);
//        spinner.setVisibility(view.GONE);
        if (!tittle.equals("")){
            textViewTittle.setText(tittle);
            RequestCreator load=Picasso.get().load(urls);
//            Picasso.get().load(urls).placeholder(R.drawable.refreshing)
//                    .error(R.drawable.refreshing)
//                    .into(imageView);
            load.into(imageView);
            imageView.refreshDrawableState();
        }

    }

    @Override
    public void onClick(View v) {
        if (listener!=null){
            listener.onClick(v);

        }
    }
    public void setOnClickListener(View.OnClickListener listenerM){
        this.listener = listenerM;

    }

    @Override
    public View getInfoWindow(Marker marker) {
        rendowWindowText(marker,mWindow);
        return mWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        String tittle = marker.getTitle();
        String p = url;
        TextView textViewTittle =  (TextView)mWindow.findViewById(R.id.title);
        ImageView imageView = (ImageView)mWindow.findViewById(R.id.idMuseum);
        String urls = marker.getSnippet();
        mWindow.setOnClickListener(this);
        //Picasso.get().load(urls);
        //Float prueba = marker.getZIndex();

//        ProgressBar spinner = view.findViewById(R.id.progressBar);
//        spinner.setVisibility(view.GONE);
        if (!tittle.equals("")){
            textViewTittle.setText(tittle);
            RequestCreator load=Picasso.get().load(urls);
//            Picasso.get().load(urls).placeholder(R.drawable.refreshing)
//                    .error(R.drawable.refreshing)
//                    .into(imageView);
            load.into(imageView);
            imageView.refreshDrawableState();
        }
        return mWindow;
    }
}
