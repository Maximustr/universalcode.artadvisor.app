package com.example.universalcodeartadvisorapp.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.universalcodeartadvisorapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class View_pager_adapter extends FragmentPagerAdapter {

    public  List<Fragment> fragmentList = new ArrayList<>();
    public  List<String> fragmentListTitles = new ArrayList<>();

    public View_pager_adapter(FragmentManager fm) {
        super(fm);
        // Required empty public constructor
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentListTitles.get(position);
    }

    @Override
    public int getCount() {
        return fragmentListTitles.size();
    }


    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    public void addFragment(Fragment fragment, String title,String idMuseum){

        fragmentList.add(fragment);
        Bundle mibundle = new Bundle();

        mibundle.putString("idMuseum",idMuseum);

        fragment.setArguments(mibundle);

        fragmentListTitles.add(title);

    }

}
