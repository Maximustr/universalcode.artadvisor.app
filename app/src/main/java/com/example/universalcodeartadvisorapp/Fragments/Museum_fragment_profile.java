package com.example.universalcodeartadvisorapp.Fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.universalcodeartadvisorapp.Pojos.Museum;
import com.example.universalcodeartadvisorapp.Pojos.Price;
import com.example.universalcodeartadvisorapp.Pojos.Schedule;
import com.example.universalcodeartadvisorapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class Museum_fragment_profile extends Fragment {

    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;

    String idMuseum;
    DocumentReference docRef;
    TextView name;
    ImageView img;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public Museum_fragment_profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.museum_fragment_profile, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();

        // Inflate the layout for this fragment
        name = (TextView) view.findViewById(R.id.idNombre);
        img = (ImageView) view.findViewById(R.id.idImg);

        tabLayout = (TabLayout) view.findViewById(R.id.tabId);
        appBarLayout = (AppBarLayout) view.findViewById(R.id.appBarId);
        viewPager = (ViewPager) view.findViewById(R.id.viewPagerId);


        if (getArguments() != null) {

            idMuseum = getArguments().getString("idMuseum");
            View_pager_adapter adapter = new View_pager_adapter(getChildFragmentManager());
            adapter.addFragment(new Profile_fragment_tab(),"Perfil",idMuseum);
            adapter.addFragment(new Comments_fragment_tab(),"Comentarios",idMuseum);
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        }

        getMuseum();
        return view;
    }

    public void getMuseum() {
        db.collection("museum")
            .whereEqualTo("idMuseum", idMuseum)
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Museum museums = document.toObject(Museum.class);
                            String id = museums.getIdMuseum();
                            name.setText(museums.getName());
                            Picasso.get().load(museums.getImg()).into(img);
                        }
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                }
            });
    }
}
