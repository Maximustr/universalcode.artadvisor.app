package com.example.universalcodeartadvisorapp.Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.universalcodeartadvisorapp.Pojos.Museum;
import com.example.universalcodeartadvisorapp.Pojos.Price;
import com.example.universalcodeartadvisorapp.Pojos.Schedule;
import com.example.universalcodeartadvisorapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class Profile_fragment_tab extends Fragment implements View.OnClickListener{

    String idMuseum;
    DocumentReference docRef;
    TextView description, schedules, prices,address,link,email,phone,points;
    ImageView imgLink,imgLinkGo, imgBackSchedule,imgSchedule,imgPrice,imgBackPrice;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseFirestore db2 = FirebaseFirestore.getInstance();
    FirebaseFirestore db3 = FirebaseFirestore.getInstance();
    ArrayList<Price> listPrices;
    public ArrayList<Schedule> listSchedules;
    Museum objMuseum;
    Dialog myDialog;
    RatingBar stars;
    RecyclerView recyclerInfoSchedules,recyclerInfoPrices;
    String latitud, longitud,nameMuseum,linkSrc;
    My_schedule_adapter adapter;

    public Profile_fragment_tab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.museum_profile_tab, container, false);

        ImageButton irAlMapa = (ImageButton) view.findViewById(R.id.idUbicacion);
        irAlMapa.setOnClickListener(this);

        schedules = (TextView) view.findViewById(R.id.idSchedule);
        schedules.setOnClickListener(this);
        prices = (TextView) view.findViewById(R.id.idPrice);
        prices.setOnClickListener(this);
        email = (TextView) view.findViewById(R.id.idEmail);
        phone = (TextView) view.findViewById(R.id.idPhone);
        stars = (RatingBar) view.findViewById(R.id.idStars);
        points = (TextView) view.findViewById(R.id.idPoints);
        imgLink = (ImageView) view.findViewById(R.id.idLinkImg);
        imgLink.setOnClickListener(this);
        imgBackSchedule = (ImageView) view.findViewById(R.id.idImgBackSchedule);
        imgBackSchedule.setOnClickListener(this);
        imgSchedule = (ImageView) view.findViewById(R.id.idImagSchedule);
        imgSchedule.setOnClickListener(this);
        imgPrice = (ImageView) view.findViewById(R.id.idImgPrice);
        imgPrice.setOnClickListener(this);
        imgBackPrice= (ImageView) view.findViewById(R.id.idImgBackPrice);
        imgBackPrice.setOnClickListener(this);
        link = (TextView) view.findViewById(R.id.idLink);
        link.setOnClickListener(this);
        imgLinkGo = (ImageView) view.findViewById(R.id.idGoLink);
        imgLinkGo.setOnClickListener(this);
        address = (TextView) view.findViewById(R.id.idAddress);
        description = (TextView) view.findViewById(R.id.idDescription);
        listSchedules = new ArrayList<Schedule>();
        listPrices = new ArrayList<Price>();

        if (getArguments() != null) {

            idMuseum = getArguments().getString("idMuseum");
        }

//        ImageButton buttonGoMap = (ImageButton) view.findViewById(R.id.idUbicacion);
//        buttonGoMap.setOnClickListener(this);

        fillPrices();
        fillSchedules();
        getMuseum();
        return view;
    }

    public void getMuseum() {
        db.collection("museum")
            .whereEqualTo("idMuseum", idMuseum)
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Museum museums = document.toObject(Museum.class);
                            String id = museums.getIdMuseum();
                            description.setText(museums.getDescription());
                            description.setMovementMethod(new ScrollingMovementMethod());
                            latitud = museums.getLatitud();
                            stars.setRating(Float.parseFloat(museums.getStars()));
                            nameMuseum = museums.getName();
                            longitud = museums.getLongitud();
                            address.setText(museums.getAddress());
                            address.setMovementMethod(new ScrollingMovementMethod());
                            link.setText(museums.getLink());
                            link.setMovementMethod(new ScrollingMovementMethod());
                            email.setText(museums.getEmail());
                            linkSrc = museums.getLink();
                            points.setText("Puntos del museo: "+museums.getPoints());
                            phone.setText("+506"+ museums.getPhoneNumber());
                            link.setMovementMethod(LinkMovementMethod.getInstance());
                        }
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                }
            });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.idUbicacion:

                MapFragment mapFragment = new MapFragment();
                goMap(mapFragment);

                break;
                case R.id.idLinkImg:

                    openLink();

                 break;

            case R.id.idGoLink:

                openLink();

                break;

            case R.id.idSchedule:

                    openDialogSchedule();
                break;

            case R.id.idImgBackSchedule:

                openDialogSchedule();

                break;

            case R.id.idImagSchedule:

                openDialogSchedule();

                break;

            case R.id.idPrice:

                    openDialogPrice();
                break;

            case R.id.idImgPrice:

                openDialogPrice();

                break;

            case R.id.idImgBackPrice:

                openDialogPrice();

                break;

        }

    }

    public void openLink(){
        Uri uri= Uri.parse(linkSrc);
        Intent intentNav = new Intent(Intent.ACTION_VIEW,uri);
        startActivity(intentNav);
    }

    public void openDialogSchedule(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        View mView = getLayoutInflater().inflate(R.layout.custompopup_schedule,null);
        TextView txtClose;
//                myDialog.setContentView(R.layout.custompopup_schedule);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.custompopup_schedule, null));
        recyclerInfoSchedules = mView.findViewById(R.id.myrecycleschedules);
        recyclerInfoSchedules.setLayoutManager(new LinearLayoutManager(getActivity()));

        My_schedule_adapter adapter = new My_schedule_adapter(listSchedules);
        adapter.setOnClickListener(this);

        recyclerInfoSchedules.setAdapter(adapter);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();



        txtClose = (TextView) mView.findViewById(R.id.txtClose);
        txtClose.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void openDialogPrice(){
        TextView txtClose;
        AlertDialog.Builder mBuilderPrice = new AlertDialog.Builder(getActivity());
        View mViewPrice = getLayoutInflater().inflate(R.layout.custompopup_price,null);
//                myDialog.setContentView(R.layout.custompopup_schedule);
        AlertDialog.Builder builderPrice = new AlertDialog.Builder(getActivity());
        LayoutInflater inflaterPrice = getActivity().getLayoutInflater();
        builderPrice.setView(inflaterPrice.inflate(R.layout.custompopup_price, null));
        recyclerInfoPrices = mViewPrice.findViewById(R.id.myrecycleprices);
        recyclerInfoPrices.setLayoutManager(new LinearLayoutManager(getActivity()));

        My_price_adapter adapterPrice = new My_price_adapter(listPrices);
        adapterPrice.setOnClickListener(this);

        recyclerInfoPrices.setAdapter(adapterPrice);

        mBuilderPrice.setView(mViewPrice);
        final AlertDialog dialogPrice = mBuilderPrice.create();
        dialogPrice.show();



        txtClose = (TextView) mViewPrice.findViewById(R.id.txtClose);
        txtClose.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                dialogPrice.dismiss();
            }
        });
    }

    public void fillPrices(){
        db3.collection("price")
            .whereEqualTo("idMuseum", idMuseum)
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task2) {
                    if (task2.isSuccessful()) {
                        for (QueryDocumentSnapshot document2 : task2.getResult()) {
                            final Price price = document2.toObject(Price.class);
                            listPrices.add(price);
                        }
                    }else {
                        Log.d(TAG, "Error getting documents: ", task2.getException());
                    }
                }
            });
    }

    public void fillSchedules(){
        db.collection("schedule")
            .whereEqualTo("idMuseum",idMuseum)
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
//                        String varschedule = "";
                        for (QueryDocumentSnapshot document : task.getResult()) {
                             Schedule schedule = document.toObject(Schedule.class);
                            listSchedules.add(schedule);
//                            varschedule = varschedule + schedule.getDescription() + ": " + schedule.getStartHour() + "-" + schedule.getFinishHour();
                        }
//                                        schedules.setText("Horarios: "+varschedule);
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                }
            });

//        listSchedules.add(new Schedule("Sabado",null,"78","76","juju"));
//        listSchedules.add(new Schedule("Sabado",null,"78","76","juju"));
//        listSchedules.add(new Schedule("Sabado",null,"78","76","juju"));
//        listSchedules.add(new Schedule("Sabado",null,"78","76","juju"));
    }

    @SuppressLint("ResourceType")
    public void goMap(Fragment mapFragment) {


        Bundle mibundle = new Bundle();
        double[ ] arrayDouble = new double[2];
        double lat = Double.parseDouble(latitud);
        double longi = Double.parseDouble(longitud);
        arrayDouble[0] = lat;
        arrayDouble[1] = longi;
        mibundle.putString("nombreRestaurante",nameMuseum);
        mibundle.putDoubleArray("coordenadas",arrayDouble);
        mibundle.putBoolean("irAlMapa",true);

        mapFragment.setArguments(mibundle);

        FragmentManager fragmentManager = getFragmentManager();
        //Replace intent with Bundle and put it in the transaction
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction
                .replace(R.id.main_frame,mapFragment)
                .commit();

    }
}
