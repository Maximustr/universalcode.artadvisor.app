package com.example.universalcodeartadvisorapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.universalcodeartadvisorapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Comments_fragment_tab extends Fragment {


    public Comments_fragment_tab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.comments_fragment_tab, container, false);
    }

}
