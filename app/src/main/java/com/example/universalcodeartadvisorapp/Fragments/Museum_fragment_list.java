package com.example.universalcodeartadvisorapp.Fragments;


import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.example.universalcodeartadvisorapp.Pojos.Museum;
import com.example.universalcodeartadvisorapp.Pojos.Price;
import com.example.universalcodeartadvisorapp.Pojos.Schedule;
import com.example.universalcodeartadvisorapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class Museum_fragment_list extends Fragment implements View.OnClickListener{

    RecyclerView recyclerInfo;
    public ArrayList<Museum> listMuseums;
    public ArrayList<Price> listPrice;
    public ArrayList<Schedule> listSchedule;
    My_museum_adapter adapter;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseFirestore db2 = FirebaseFirestore.getInstance();
    FirebaseFirestore db3 = FirebaseFirestore.getInstance();
    DocumentReference docRef ;
    ProgressBar progressBar;
    WebView webView;
    public Fragment museumFragment;

    public Museum_fragment_list() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.museum_fragment_list, container, false);

        museumFragment = new Museum_fragment_list();

        listMuseums = null;

        listMuseums = new ArrayList<Museum>();


        recyclerInfo = view.findViewById(R.id.myrecyclemuseums);
//        progressBar = view.findViewById(R.id.idProgressBar);
//        webView = view.findViewById(R.id.webView);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.setWebViewClient(new WebViewClient(){
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                super.onPageStarted(view, url, favicon);
//                progressBar.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                progressBar.setVisibility(View.GONE);
//            }
//        });
        recyclerInfo.setLayoutManager(new LinearLayoutManager(getActivity()));


        fillList();
        adapter = new My_museum_adapter(listMuseums);
        adapter.setOnClickListener(this);

        recyclerInfo.setAdapter(adapter);

        return view;
    }

    @Override
    public void onClick(View v) {

        Museum_fragment_profile museumProfile = new Museum_fragment_profile();

        String idMuseum;
        idMuseum =listMuseums.get(recyclerInfo.getChildAdapterPosition(v)).getIdMuseum();
        goMuseum(museumProfile,idMuseum);

}
    @SuppressLint("ResourceType")
    public void goMuseum(Fragment museumProfile, String idMuseum) {


        Bundle mibundle = new Bundle();
        mibundle.putString("idMuseum",idMuseum);
        museumProfile.setArguments(mibundle);
        FragmentManager fragmentManager = getFragmentManager();
        //Replace intent with Bundle and put it in the transaction
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.main_frame,museumProfile);
        fragmentTransaction.commit();
    }
    public  void fillList() {
//        listMuseums.add(new Museum("nombre","Description"));
        db.collection("museum")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            final Museum museums = document.toObject(Museum.class);
                            listMuseums.add(museums);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }
}
