package com.example.universalcodeartadvisorapp.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.universalcodeartadvisorapp.Pojos.Museum;
import com.example.universalcodeartadvisorapp.Pojos.Schedule;
import com.example.universalcodeartadvisorapp.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class My_schedule_adapter extends RecyclerView.Adapter<My_schedule_adapter.ViewHolder> implements View.OnClickListener {

    public ArrayList<Schedule> listSchedules;
    private View.OnClickListener listener;

    public My_schedule_adapter(final ArrayList listSchedules) {
        // Required empty public constructor
      this.listSchedules = listSchedules;
    }

    @NonNull
    @Override
    public My_schedule_adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.schedules_rows,null, false);

        view.setOnClickListener(this);
        return new My_schedule_adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull My_schedule_adapter.ViewHolder viewHolder, int i) {
        viewHolder.infoSchedules.setText(listSchedules.get(i).getDescription()+": "+ listSchedules.get(i).getStartHour() +"-"+ listSchedules.get(i).getFinishHour());
    }

    @Override
    public int getItemCount() {
        return listSchedules.size();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.custompopup_schedule, container, false);
    }

    @Override
    public void onClick(View v) {
        if (listener!=null){
            listener.onClick(v);

        }
    }
    public void setOnClickListener(View.OnClickListener listenerM){
        this.listener = listenerM;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView infoSchedules = null;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            infoSchedules = itemView.findViewById(R.id.idInfoSchedule);
        }
    }
}
