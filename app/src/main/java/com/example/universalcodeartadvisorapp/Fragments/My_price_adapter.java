package com.example.universalcodeartadvisorapp.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.universalcodeartadvisorapp.Pojos.Price;
import com.example.universalcodeartadvisorapp.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class My_price_adapter extends RecyclerView.Adapter<My_price_adapter.ViewHolder> implements View.OnClickListener {

    public ArrayList<Price> listPrices;
    private View.OnClickListener listener;

    public My_price_adapter(final ArrayList listPrices) {
        // Required empty public constructor
        this.listPrices = listPrices;
    }

    @NonNull
    @Override
    public My_price_adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.prices_rows,null, false);

        view.setOnClickListener(this);
        return new My_price_adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull My_price_adapter.ViewHolder viewHolder, int i) {
        viewHolder.infoPrices.setText(listPrices.get(i).getDescription()+": "+ listPrices.get(i).getPrice());
    }

    @Override
    public int getItemCount() {
        return listPrices.size();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.custompopup_price, container, false);
    }

    @Override
    public void onClick(View v) {
        if (listener!=null){
            listener.onClick(v);

        }
    }
    public void setOnClickListener(View.OnClickListener listenerM){
        this.listener = listenerM;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView infoPrices = null;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            infoPrices = itemView.findViewById(R.id.idInfoPrices);
        }
    }
}
