package com.example.universalcodeartadvisorapp.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.icu.text.DecimalFormat;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.universalcodeartadvisorapp.Common.CurrentUser;
import com.example.universalcodeartadvisorapp.Pojos.Museum;
import com.example.universalcodeartadvisorapp.Pojos.MuseumVisited;
import com.example.universalcodeartadvisorapp.Pojos.User;
import com.example.universalcodeartadvisorapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.support.constraint.Constraints.TAG;


public class MapFragment extends Fragment implements GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback,
        View.OnClickListener{

    public MapFragment() {
        // Required empty public constructor
    }
    private GoogleMap mMap;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseFirestore db1 = FirebaseFirestore.getInstance();
    public ArrayList<Museum> listMuseum;
    public ArrayList<MuseumVisited> listMuseumVisited;
    public ArrayList<MuseumVisited> listMuseumVisitedUser;
    public ArrayList<MuseumVisited> listMuseumVisitedUserMarker;
    boolean desdeElRest = false;
    double latActualUser, longiActualUser;
    private LocationManager locManager;
    private Location loc;
    User userActual;
    String image;


    private Map<Marker, Class<Museum>> allMarkersMap = new HashMap<Marker, Class<Museum>>();





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);
        listMuseum = null;
        listMuseumVisited = null;
        listMuseumVisitedUser = null;
        listMuseumVisitedUserMarker = null;

        userActual = CurrentUser.user;
        listMuseumVisited  = new ArrayList<MuseumVisited>();
        listMuseum = new ArrayList<Museum>();
        listMuseumVisitedUser = new ArrayList<MuseumVisited>();
        listMuseumVisitedUserMarker = new ArrayList<MuseumVisited>();

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (getArguments()!=null){

            String nombre = getArguments().getString("nombreRestaurante");

            double[ ] arrayDouble = new double[2];
            arrayDouble = getArguments().getDoubleArray("coordenadas");
            desdeElRest= getArguments().getBoolean("irAlMapa");
            LatLng sydney = new LatLng(arrayDouble[0], arrayDouble[1]);
            mMap.addMarker(new MarkerOptions().position(sydney).title(nombre));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));

        }

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        loc = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        latActualUser = loc.getLatitude();
        longiActualUser = loc.getLongitude();
        if (desdeElRest==false){
            listMuseum= new ArrayList<Museum>();
        }
        mostrarMuseos();

        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);

    }
    public void mostrarMuseos(){
        listMuseum= new ArrayList<Museum>();

        //museumVisited
        CollectionReference museumVisitedCollection = db1.collection("museumVisited");

        museumVisitedCollection.orderBy("idMuseum")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                MuseumVisited museumVisited = document.toObject(MuseumVisited.class);

                                if (museumVisited!=null){
                                    listMuseumVisited.add(museumVisited);

                                }
                            }
                            for (final MuseumVisited museumVisitedUser : listMuseumVisited) {
                                if(museumVisitedUser.getIdUser().equals(userActual.getIdUser())){
                                    listMuseumVisitedUser.add(museumVisitedUser);
                                }
                            }
                            //
                            CollectionReference museumCollection = db.collection("museum");
                            museumCollection.orderBy("idMuseum", Query.Direction.ASCENDING)
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                for (QueryDocumentSnapshot document : task.getResult()) {
                                                    Museum museums = document.toObject(Museum.class);
                                                    listMuseum.add(museums);
                                                }
                                                agregarPin(listMuseum,listMuseumVisitedUser);
                                            } else {
                                                Log.d(TAG, "Error getting documents: ", task.getException());
                                            }
                                        }
                                    });
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    //#########################

    private void agregarPin(final ArrayList<Museum> listMuseum, final ArrayList<MuseumVisited> listMuseumVisited) {
        Boolean entroAlIf = false;

        if (listMuseumVisited.size()!=0){
//            for ( final MuseumVisited museumVisited: listMuseumVisited ){
                    for (final Museum museu : listMuseum) {
//                        String idMuseu, museoVisitado;
//                        idMuseu = museu.getIdMuseum();
//                        museoVisitado = museumVisited.getIdMuseum();
//
//                        if (entroAlIf==false) {
//                            MuseumVisited museumVisiteds = new MuseumVisited();
//                            museumVisiteds.setIdMuseum(idMuseu);
//                            museumVisiteds.setIdUser(museumVisited.getIdUser());
//                            listMuseumVisitedUserMarker.add(museumVisiteds);
//                        }

//                        if (museoVisitado.equals(idMuseu)) {
                            double lat = Double.parseDouble(museu.getLatitud());
                            double longi = Double.parseDouble(museu.getLongitud());

                            LatLng sydney = new LatLng(lat, longi);
                            Marker marker = mMap.addMarker(new MarkerOptions()
                                    .position(sydney)
                                    .title(museu.getName())
                                    .snippet(museu.getImg())
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.museovisitadoazul)));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                            allMarkersMap.put(marker, Museum.class);
                            image = museu.getImg();
//                        }
                    }
                    entroAlIf = true;
//            }

//            CollectionReference museumCollection = db.collection("museum");
//          //  museumCollection .whereEqualTo("idMuseum", idMuseum)
//                    .get()
//                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                        @Override
//                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                            if (task.isSuccessful()) {
//                                for (QueryDocumentSnapshot document : task.getResult()) {
//                                    Museum museums = document.toObject(Museum.class);
//                                    listMuseum.add(museums);
//                                }
//                                agregarPin(listMuseum,listMuseumVisitedUser);
//                            } else {
//                                Log.d(TAG, "Error getting documents: ", task.getException());
//                            }
//                        }
//                    });

//            if (listMuseumVisitedUserMarker.size()!=0){
//                for (final MuseumVisited museu : listMuseumVisited  ){
//                    for (final MuseumVisited museumVisited: listMuseumVisitedUserMarker) {
//
//                        String idMuseu, museoVisitado;
//                        idMuseu = museu.getIdMuseum();
//                        museoVisitado = museumVisited.getIdMuseum();
//
//                        if (!museoVisitado.equals(idMuseu)) {
//                            MuseumVisited museumVisiteds = new MuseumVisited();
//                            museumVisiteds.setIdMuseum(museu.getIdMuseum());
//                            museumVisiteds.setIdUser(museumVisited.getIdUser());
//                            listMuseumVisitedUserMarker.add(museumVisiteds);
//                        double lat = Double.parseDouble(museu.getLatitud());
//                        double longi = Double.parseDouble(museu.getLongitud());
//
//                        LatLng sydney = new LatLng(lat, longi);
//                        Marker marker = mMap.addMarker(new MarkerOptions()
//                                .position(sydney)
//                                .title(museu.getName())
//                                .snippet(museu.getImg())
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.museonovisitadonegro)));
//                        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//                        allMarkersMap.put(marker, Museum.class);
//                        image = museu.getImg();

//
//                        }
//
//                    }
//                }
//
//            }

        }else{
            for (final Museum museu: listMuseum) {
                double lat = Double.parseDouble(museu.getLatitud());
                double longi = Double.parseDouble(museu.getLongitud());

                LatLng sydney = new LatLng(lat, longi);
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(sydney)
                        .title(museu.getName())
                        .snippet(museu.getImg())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.museonovisitadonegro)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                allMarkersMap.put(marker, Museum.class);
                image = museu.getImg();

            }
        }

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {

                    Double latMarker, longMarker, longiActual, latActual;
                    latMarker = marker.getPosition().latitude;
                    longMarker = marker.getPosition().longitude;

                    longiActual = longiActualUser;
                    latActual = latActualUser;

                    final Double distancias = obtenerDistancia(latActual,longiActual,latMarker,longMarker);
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                    final View mView = getLayoutInflater().inflate(R.layout.custom_address,null);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.CustomDialog);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    builder.setView(inflater.inflate(R.layout.custom_address, null));
                    TextView mainTitle = mView.findViewById(R.id.title);
                    mainTitle.setText(marker.getTitle());
                    TextView distan = mView.findViewById(R.id.distan);
                    distan.setText("Distancia: "+String.format("%.2f", (distancias))+" KM");
                    String image = marker.getSnippet();
                    ImageView imgModal = mView.findViewById(R.id.idMuseum);
                    Picasso.get().load(image)
                            .into(imgModal);

                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();


                    //Cerrar modal
                ImageButton btnCerrar = mView.findViewById(R.id.cerrarModal);
                btnCerrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                //Marcar posicion
                    ImageButton btnMarcarVisita= mView.findViewById(R.id.marcarVisita);
                    btnMarcarVisita.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            agregarAFirebase(marker.getTitle(),marker.getPosition().latitude,marker.getPosition().longitude);

                        }
                    });

                    return true;
                }
            });

    }

    public void agregarAFirebase(final String nameMuseum, double latMarker, double longiMarker){
        final Map<String, Object> data = new HashMap<>();
        String idMuseums = null;
        final String idUser = userActual.getIdUser();
        Boolean museoEstaRegistradoVisitado = false;

        for (MuseumVisited museumVisited : listMuseumVisitedUser) {
            if (museumVisited.getName().equals(nameMuseum)) {
                Toast.makeText(getActivity(), nameMuseum +
                                " ya se encuentra visitado",
                        Toast.LENGTH_SHORT).show();
                museoEstaRegistradoVisitado = true;


            }
        }

        if (museoEstaRegistradoVisitado==false){
            for (Museum museum:listMuseum){
                if (museum.getName().equals(nameMuseum)){
                    idMuseums = museum.getIdMuseum();

                }
            }

            double distancia = obtenerDistancia(latActualUser,longiActualUser,latMarker,longiMarker);
            String dis = String.format("%.2f", (distancia));

            double distanciaDouble = Double.parseDouble(dis);
            if (distanciaDouble<=1){
                data.put("idMuseum", idMuseums);
                data.put("idUser", idUser);
                data.put("name", nameMuseum);
                db1.collection("museumVisited")
                        .add(data)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding document", e);
                            }
                        });
                Toast.makeText(getActivity(),"Se agrego el: "+
                                nameMuseum+
                                " como visitado",
                        Toast.LENGTH_SHORT).show();
                mostrarMuseos();

            }else{
                Toast.makeText(getActivity(), "El museo se encuentra lejos de su ubicación, no se puede agregar.",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    public double obtenerDistancia(double actualUbicaciónLat, double actualUbicaciónLon, double markerLat, double markerLong){
        double distancia=0;
        double radioTierra = 6371;//en kilómetros
        double dLat = Math.toRadians(markerLat - actualUbicaciónLat);
        double dLng = Math.toRadians(markerLong - actualUbicaciónLon);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(actualUbicaciónLat)) * Math.cos(Math.toRadians(markerLat));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        distancia = radioTierra * va2;

        return distancia;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        //mostrarMuseos();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length == 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mostrarMuseos();
//                    LatLng sydney = new LatLng(9.9333296, -84.0833282);
//                    mMap.addMarker(new MarkerOptions().position(sydney).title("San José"));
//                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
//                    mostrarMuseos();


                    mMap.setMyLocationEnabled(true);


                } else {
                    Toast.makeText(getActivity(), "No se va a mostrar la ubicación actual, por favor activar los permisos.", Toast.LENGTH_LONG ).show();
                    mostrarMuseos();
                }

            }
        }
    }




    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        // Retrieve the data from the marker.
        Integer clickCount = (Integer) marker.getTag();

        // Check if a click count was set, then display the click count.
        if (clickCount != null) {
            clickCount = clickCount + 1;
            marker.setTag(clickCount);
            Toast.makeText(getActivity(),
                    marker.getTitle() +
                            " has been clicked " + clickCount + " times.",
                    Toast.LENGTH_SHORT).show();
        }

        // Return false to indicate that we have not consumed the event and that we wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return false;
    }

    @Override
    public void onClick(View v) {

    }


    //####################




}
