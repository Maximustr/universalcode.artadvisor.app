package com.example.universalcodeartadvisorapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.universalcodeartadvisorapp.Activitys.LogIn;
import com.example.universalcodeartadvisorapp.Activitys.UpdateProfile;
import com.example.universalcodeartadvisorapp.Common.CurrentUser;
import com.example.universalcodeartadvisorapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class MyAccount extends Fragment {

    private View rootView;

    public MyAccount() {
    }


    private void FillMyAccount() {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference ref = storage.getReference().child(CurrentUser.user.getPhotoUrl());

        ref.getDownloadUrl().addOnSuccessListener(uri -> {
            Picasso.get().load(uri.toString()).into((ImageView) rootView.findViewById(R.id.MyPhoto));
            CurrentUser.user.setPhotoUrlToDownload(uri.toString());
        });
        TextView myAwesomeTextView = rootView.findViewById(R.id.FullNameOfUser);

        String fullName = CurrentUser.user.getName() + " " + CurrentUser.user.getLastName();
        myAwesomeTextView.setText(fullName);
        TextView email = rootView.findViewById(R.id.EmailTxt);
        email.setText(CurrentUser.user.getEmail());
        TextView countryCodeTxt = rootView.findViewById(R.id.CountryCodeTxt);
        countryCodeTxt.setText(CurrentUser.user.getNationality());
    }

    @Override
    public void onStart() {
        super.onStart();
        FillMyAccount();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.fragment_my_account, container, false);
        return rootView;
    }


    public void SignOut(View view) {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getActivity(), LogIn.class);
        startActivity(intent);
        getActivity().finish();
    }

    public void EditProfile(View view) {
        Intent intent = new Intent(getActivity(), UpdateProfile.class);
        startActivity(intent);
    }
}
