package com.example.universalcodeartadvisorapp.Fragments;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.universalcodeartadvisorapp.Pojos.Museum;
import com.example.universalcodeartadvisorapp.Pojos.Schedule;
import com.example.universalcodeartadvisorapp.R;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class My_museum_adapter  extends RecyclerView.Adapter<My_museum_adapter.ViewHolder> implements View.OnClickListener  {

    public ArrayList<Museum> listMuseums;
    private View.OnClickListener listener;


    public My_museum_adapter(final ArrayList listMuseums) {
        this.listMuseums = listMuseums;
    }

    @NonNull
    @Override
    public My_museum_adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.museums_rows,null, false);

        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull My_museum_adapter.ViewHolder holder, int i) {
        String schedules = "";
        holder.myTextName.setText(listMuseums.get(i).getName());
//        for (Schedule schedule: listMuseums.get(i).getSchedules()) {
//            schedules = schedules + schedule.getDescription()+": "+ schedule.getStartHour() +"-"+ schedule.getFinishHour();
//        }
//        holder.myTextSchedule.setText("Horarios: "+schedules);
        Picasso.get().load(listMuseums.get(i).getImg())
            .placeholder(R.drawable.museodorsai)
            .into(holder.myTextImg);

//        Glide.with().load(listMuseums.get(i).getImg()).into(holder.myTextImg);
//        holder.myTextImg.setImageResource(Integer.parseInt(listMuseums.get(i).getImg()));
//        Picasso.with(this).load(listMuseums.get(i).getImg()).fit().into(holder.myTextImg);
//        holder.myTextImg.setText(Glide.with().load(listMuseums.get(i).getImg()));
//        holder.myTextImg.
//        (Uri.parse(listMuseums.get(i).getImg()));

    }

    @Override
    public int getItemCount() {
        return listMuseums.size();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.museum_fragment_list, container, false);
    }

    @Override
    public void onClick(View v) {
        if (listener!=null){
            listener.onClick(v);

        }
    }
    public void setOnClickListener(View.OnClickListener listenerM){
        this.listener = listenerM;

    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView myTextName = null;
//        TextView myTextSchedule = null;
        ImageView myTextImg = null;


        public ViewHolder(View itemview){
            super(itemview);
            myTextName = itemview.findViewById(R.id.idNombre);
//            myTextSchedule = itemview.findViewById(R.id.idSchedule);
            myTextImg = itemview.findViewById(R.id.idImg);
        }
    }
}

