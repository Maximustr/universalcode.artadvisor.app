package com.example.universalcodeartadvisorapp.Pojos;

public class Schedule {

    private String idSchedule;
    private String idMuseum;
    private String startHour;
    private String finishHour;
    private String description;


    public Schedule(){

    }
    public Schedule(String idSchedule, String idMuseum, String startHour, String finishHour,String description){
        this.idSchedule = idSchedule;
        this.idMuseum = idMuseum;
        this.startHour = startHour;
        this.finishHour = finishHour;
        this.description = description;
    }

    public String getIdMuseum() {
        return idMuseum;
    }

    public void setIdMuseum(String idMuseum) {
        this.idMuseum = idMuseum;
    }

    public String getIdSchedule() {
        return idSchedule;
    }

    public void setIdSchedule(String idSchedule) {
        this.idSchedule = idSchedule;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getFinishHour() {
        return finishHour;
    }

    public void setFinishHour(String finishHour) {
        this.finishHour = finishHour;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
