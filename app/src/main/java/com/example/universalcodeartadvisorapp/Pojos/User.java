package com.example.universalcodeartadvisorapp.Pojos;

import com.google.firebase.firestore.Exclude;

import java.lang.reflect.Field;
import java.util.Map;

public class User {

    private String idUser;
    private String name;
    private String lastName;
    private String phoneNumber;
    private String email;
    @Exclude
    private String password;
    private String nationality;
    private String photoUrl;

    @Exclude
    private String photoUrlToDownload;

    public User() {
    }

    public User(Map<String, Object> objectMap) {
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                field.set(this, objectMap.get(field.getName()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public User(String idUse, String name, String lastName, String phoneNumber, String email, String password, String nationality, String photoUrl, String photoUrlToDownload) {
        idUser = idUse;
        this.name = name;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.nationality = nationality;
        this.photoUrl = photoUrl;
        this.photoUrlToDownload = photoUrlToDownload;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Exclude
    public String getPassword() {
        return password;
    }

    @Exclude
    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoUrlToDownload() {
        return photoUrlToDownload;
    }

    public void setPhotoUrlToDownload(String photoUrlToDownload) {
        this.photoUrlToDownload = photoUrlToDownload;
    }
}
