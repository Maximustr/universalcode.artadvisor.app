package com.example.universalcodeartadvisorapp.Pojos;


import java.io.Serializable;
import java.util.List;

public class Museum implements Serializable {

    private String img;
    private String name;
    private String description;
    private String longitud;
    private String latitud;
    private String idMuseum;
    private List<Price> prices;
    private List<Schedule> schedules;
    private String address;
    private String phoneNumber;
    private String link;
    private String email;
    private String stars;
    private String points;

    public Museum(){

    }

    public Museum(String name, String description){
        this.name = name;
        this.description = description;
    }

    public Museum(String img, String name, String description, String longitud, String latitud, String idMuseum, List<Price> prices, List<Schedule> schedules,String address,String phoneNumber,String link,String email,String stars,String points) {
        this.img = img;
        this.name = name;
        this.description = description;
        this.longitud = longitud;
        this.latitud = latitud;
        this.idMuseum = idMuseum;
        this.prices = prices;
        this.schedules = schedules;
        this.link = link;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.email = email;
        this.stars = stars;
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getIdMuseum() {
        return idMuseum;
    }

    public void setIdMuseum(String idMuseum) {
        this.idMuseum = idMuseum;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
