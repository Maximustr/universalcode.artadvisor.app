package com.example.universalcodeartadvisorapp.Pojos;

import java.io.Serializable;

public class MuseumVisited implements Serializable {

    private String idMuseum;
    private String idUser;
    private String name;

    public MuseumVisited(){}

    public String getIdMuseum() {
        return idMuseum;
    }


    public void setIdMuseum(String idMuseum) {
        this.idMuseum = idMuseum;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
