package com.example.universalcodeartadvisorapp.Pojos;

public class Price {

    private String idPrice;
    private Museum objMuseum;
    private String description;
    private String price;

    public Price(){

    }

    public Price(String idPrice, Museum objMuseum, String description, String price) {
        this.idPrice = idPrice;
        this.objMuseum = objMuseum;
        this.description = description;
        this.price = price;
    }

    public String getIdPrice() {
        return idPrice;
    }

    public void setIdPrice(String idPrice) {
        this.idPrice = idPrice;
    }

    public Museum getObjMuseum() {
        return objMuseum;
    }

    public void setObjMuseum(Museum objMuseum) {
        this.objMuseum = objMuseum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
